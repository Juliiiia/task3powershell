﻿Describe "Owl" {
   
    it "Owl" {
 
     $result = owl -go_to_sleep  23
     $result | Should Be "You are owl"
 
    }
 }
 Describe "Owl_n" {
   
    it "Owl" {
 
     $result = owl -go_to_sleep  20
     $result | Should Be "You are not owl"
 
    }
 }

 Describe "Lark" {
   
    it "Lark" {
 
     $result = lark -wake_up  5
     $result | Should Be "You are lark"
 
    }
}
Describe "Lark_n" {
    it "Lark" {
     $result = lark -wake_up  8
     $result | Should Be "You are not lark"
 
    }
}
Describe "Gettimes" {
    it "Gettimes" {
     $result = Gettimes -wake_up  8 -go_to_sleep 22
     $Wa =8
     $Gs = 22
    [datatime]$result1 =  $start_span Get-Date -Hour $Wa 
    $result2 = [datetime] $global:end_span = Get-Date -Hour $Gs

     $result | Should Be $result1
 
    }
}