﻿<#

.SYNOPSIS
This is a simple Powershell script which help contpoll you sleep 

.DESCRIPTION
Function this skripts:
-user passes time he/she goes to bed and wakes up at;
-depending on that info script writes whether the user is an owl or a lark; 
-script also writes if the user has enough sleep (8 hours or more) 
- use try-catch blocks to process input 



.EXAMPLE
./3_final.ps1

.NOTES
Script wrote on powershell5

.LINK
.

#>



param ( 
    [validaterange(0, 23)]$go_to_sleep ,
    [validaterange(0, 23)]$wake_up ,
    [string]$Users,
    [string]$Second_name, 
    [int]$age 


)
function Write-Log {
    Param (   
        [Parameter(Mandatory = $false)] 
        [string]$Level,     
        [Parameter(Mandatory = $false)]    
        [string]$Message, 
        [Parameter(Mandatory = $false)] 
        [switch]$NoClobber, 
        [Parameter(Mandatory = $false)] 
        [Alias('LogPath')] 
        [string]$Path = 'C:\Users\Yuliia_Tretiakova\skripts\task3\PowerShellLog.log'
    ) 
    Begin { 
        $VerbosePreference = 'Continue' 
    } 
    Process {   
        if ((Test-Path $Path) -AND $NoClobber) { 
            Write-Error "Log file $Path already exists, and you specified NoClobber. Either delete the file or specify a different name." 
            Return 
        } 
        elseif (!(Test-Path $Path)) { 
            Write-Verbose "Creating $Path." 
            $NewLogFile = New-Item $Path -Force -ItemType File 
        } 
        else {      
        }  
        $FormattedDate = Get-Date -Format "yyyy-MM-dd HH:mm:ss" 
        switch ($Level) { 
            'Error' { 
                Write-Warning $Message  
                $LevelText = 'ERROR:' 
            } 
            'Verbose' { 
                $Message 
                $LevelText = 'VERBOSE' 
            } 
            'Info' { 
                $Message 
                $LevelText = 'INFO' 
            } 
        }    
        "$FormattedDate $LevelText $Message" | Out-File -FilePath $Path -Append 
    } 
    End { 
    } 
}
function global:Enter {


    Write-Output($t1 + "  (Verbose)  " + "enter parametes time") >> $save
    try {
        [validaterange(0, 23)]$global:go_to_sleep = $(Read-Host "Enter Hour evening") 
        [validaterange(0, 23)]$global:wake_up = $(Read-Host "Enter Hour morning") 
        [string]$global:Users = $(Read-Host "input name" )
        [string]$global:Second_name = $(Read-Host "input second name" )
        [int]$global:age = $(Read-Host "input age" )
        Write-Log  -Level 'Verbose' -Message  'Enter parameters : go_to_sleep,wake_up '
        return $TRUE
    } 
    catch [Exception ] {
 
        $t1 = ((Get-Date).ToLocalTime()).ToString("yyyy-MM-dd HH-mm-ss")
        $save = $env:USERNAME 
        Write-Output ( $t1 + "   (Error) " + "Incorrect parameters were entered (letters or number out of range)") >> $save   
        Write-Error $_.Exception.Message 
        $err = Write-Output "Incorrect parameters were entered (letters or number out of range)"
        Write-Log  -Level 'Error' -Message  $err
    }
}
function Gettimes () { 
    
    [datetime]$start_span = Get-Date -Hour $wake_up 
    [datetime]$end_span = Get-Date -Hour $go_to_sleep
    Write-Log -Level 'Info' -Message  ('wake_up : ' + $wake_up + '   go_to_sleep :' + $go_to_sleep  )
    
    
}
function Sleepeighthout() {
    if ($go_to_sleep -ge 22 -and $wake_up -le 7  ) { 
        $sleep = "You need sleep more" 
    }
    else { 
        $sleep = "You need sleep is normal" 
    }
    write-Log  -Level 'Info' -Message  ( "You need more sleep"  )
}
function Lark() {
    param ($wake_up)

    if ($wake_up -le 7) {
        
        Write-Host ("You are lark")
        write-Log  -Level 'Info' -Message  ( "You are lark"  )
    }
    else {
        Write-Host ("You are not lark")
        write-Log  -Level 'Info' -Message  ( "You are not lark"  )
    }
}
function Owl() {
    param ($go_to_sleep)

    if ($go_to_sleep -ge 22) {
        Write-Host ("You are owl")
        write-Log  -Level 'Info' -Message  ( "You are owl"  )
    }
    else {
        Write-Host ("You are not not owl")
        write-Log  -Level 'Info' -Message  ( "You are not owl"  )
    }
}
function Exportdata { 
    [string]$Gotossv = @{ }
    $results = @()
    #$Gotossv = @($wake_up, $go_to_slee)
    $hostname = 'hostname'
    $results = New-Object -TypeName PSObject -Property @{ wake_up = $wake_up  ; go_to_sleep = $go_to_sleep ; Users = $Users ; Second_name = $Second_name; age = $age  }
    $res = $results 
    $res | Export-Csv -Append -NoTypeInformation "test.csv"
}
function Importdata {
    $P = Import-Csv -Path .\newtest.csv 
    $P
}

    Enter
    Write-Log  -Level 'Verbose' -Message  'Get time'
    Gettimes    
    Write-Log  -Level 'Verbose' -Message  'Is he/she sleep 8 hour?'
    Sleepeighthout
    Write-Log  -Level 'Verbose' -Message  'Is he/she are lark?'
    Lark
    Write-Log  -Level 'Verbose' -Message  'Is he/she are owl?'
    Owl
    Exportdata
        

#}

